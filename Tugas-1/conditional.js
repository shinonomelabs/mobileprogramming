console.log('======= IF else =========');
var nama = "Rudiana"
var peran = "Gunner"


if (nama == "" && peran == ""){
    console.log ("Nama harus diisi")
}
else if (nama == "John" && peran == ""){
    console.log ("Halo John, Pilih peranmu untuk memulai game!")
}
else if (nama == "Jane" ){
    console.log ("Selamat datang di Dunia Werewolf, Jane")
        if (peran == "Magician"){
            console.log ("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
        }
}
else if (nama == "Rudiana"){
    console.log ("Selamat datang di Dunia Werewolf, Rudiana")
    if (peran == "Gunner"){
        console.log ("Halo Rudiana Sama, kamu akan membantu melindungi temanmu dari serangan werewolf.")
    }
}

else {
    console.log ("Selamat datang di Dunia Werewolf, Rudi")
    if (peran == "Werewolf"){
        console.log ("Halo Werewolf Rudi, Kamu akan memangsa wamita setiap malam!" )
    }
}




console.log('\n\n');
console.log('======= Switch & Case =========');
var tanggal = "2";

var bulan = 3;

var tahun = "1996";

switch(bulan) {
    case 1:
        bulan = "january";
    break;
    case 2:
        bulan = "february";
    break;
    case 3:
       bulan = "maret";
    break;
    case 4:
        bulan = "april";
    break;
    case 5:
        bulan = "mei";
    break;
    case 6:
        bulan ="juni";
    break;
    case 7:
        bulan = "july";
    break;
    case 8:
        bulan = "agustus";
    break;
    case 9:
        bulan = "september";
    break;
    case 10:
        bulan = "oktober";
    break;
    case 11:
        bulan = "november";
    break;
    case 12:
        bulan = "desember";
    break;
    
  }

  console.log(tanggal + " " + bulan + " " + tahun);