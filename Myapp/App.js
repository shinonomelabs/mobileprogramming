
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Loginform from './componen/Loginform';
import Biodata from './componen/Biodata';

export default function App() {
  return (
    <View style={styles.container}>
      <Biodata />

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
});
