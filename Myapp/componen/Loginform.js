
import React from 'react';
import { 
    StyleSheet, 
    Text, 
    View,
    TextInput,
    TouchableOpacity
 } from 'react-native';

export default function Loginform() {
  return (
    <View style={styles.loginform}>
      <Text style={styles.textshow}>Username</Text>
      <TextInput style={styles.TextInput} placeholder="Username"
      underlineColorAndroid={'transparent'} />
      <Text style={styles.textshow}>Password</Text>
      <TextInput style={styles.TextInput} placeholder="Username"
      underlineColorAndroid={'transparent'} />

      <TouchableOpacity style={styles.button}>
          <Text style={styles.buttontext}>Login</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  loginform: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'left',
    justifyContent: 'center',
    
  },
  textshow: {
    color: '#fff',
    paddingBottom: 5,
    paddingTop: 5
  },
  TextInput: {
    color: '#fff',
  },
  button: {
      marginTop: 20,
      padding: 5,
      backgroundColor: '#fff',
      alignItems: 'center'
  },
  buttontext: {
      color: 'black'
  }
});
